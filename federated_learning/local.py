import warnings

import numpy as np
import torch
from sklearn.metrics import f1_score
from torch.utils.data import DataLoader
from tqdm.auto import tqdm

from utils.results_writer import ResultsWriter


class LocalModels:
    """
    Train local models for each client without federation

        Parameters:
            model_class: a torch Module to use as the model
            loss: loss function to use
            optimizer_fn: function that returns an optimizer, given model parameters
            rounds: number of rounds to train
            epochs: number of epochs to train (effective number of epochs is rounds * epochs)
            device: device to train on
    """

    def __init__(self, model_class, loss, optimizer_fn, rounds: int, epochs: int, device="cpu"):
        self.model_class = model_class
        self.loss = loss
        self.optimizer_fn = optimizer_fn
        self.rounds = rounds
        self.epochs = epochs
        self.device = device
        self.results = ResultsWriter()

    def fit(self, train_data: list[DataLoader], test_data: list[DataLoader] = None) -> ResultsWriter:
        n_clients = len(train_data)

        init_state = dict(self.model_class().named_parameters())

        for k in tqdm(range(n_clients), desc="Clients", position=1):
            model = self.model_class().to(self.device)
            model.load_state_dict(init_state, strict=False)  # reset state
            optimizer = self.optimizer_fn(model.parameters())

            for r in range(self.rounds):
                train_loss = 0
                for e in range(self.epochs):
                    train_loss += self._train_client_epoch(model, train_data[k], optimizer)

                self.results.write(
                    round=r,
                    client=str(k),
                    stage="train",
                    loss=train_loss / self.epochs,
                    n_samples=len(train_data[k].dataset)
                )

                if test_data is not None:
                    test_loss, f1 = self._test_client_epoch(model, test_data[k])
                    if test_loss is None or np.isnan(test_loss):
                        warnings.warn(f"Test loss is undefined for client {k} in round {r}")
                    if f1 is None or np.isnan(f1):
                        warnings.warn(f"F1 is undefined for client {k} in round {r}")
                    self.results.write(
                        round=r,
                        client=str(k),
                        stage="test",
                        loss=test_loss,
                        f1=f1,
                        n_samples=len(test_data[k].dataset)
                    )

        return self.results

    def _train_client_epoch(self, model, train_dataloader, optimizer) -> float:
        model.train()

        epoch_loss = 0

        for X, y in train_dataloader:
            X, y = X.to(self.device), y.to(self.device)

            pred = model(X)

            loss = self.loss(pred, y)
            epoch_loss += loss.cpu().item()

            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

        return epoch_loss / len(train_dataloader)

    def _test_client_epoch(self, model, test_dataloader) -> tuple[float, float]:
        epoch_loss = 0
        epoch_y_true = []
        epoch_y_pred = []

        model.eval()

        for X, y in test_dataloader:
            X, y = X.to(self.device), y.to(self.device)

            with torch.no_grad():
                pred = model(X)
                loss = self.loss(pred, y)
                epoch_loss += loss.cpu().item()

            epoch_y_pred.append(pred.argmax(1).detach().cpu())
            epoch_y_true.append(y.argmax(1).detach().cpu())

        epoch_loss /= len(test_dataloader.dataset)
        epoch_y_pred = np.concatenate(epoch_y_pred)
        epoch_y_true = np.concatenate(epoch_y_true)
        assert len(epoch_y_pred) == len(epoch_y_true)
        assert len(epoch_y_pred) == len(test_dataloader.dataset)

        return epoch_loss, f1_score(epoch_y_true, epoch_y_pred, average="macro")
